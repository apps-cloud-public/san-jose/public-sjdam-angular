import { NgModule } from '@angular/core';
import { LolRoutingModule } from './lol-routing.module';
import { LolComponent } from './page-lol/page-lol.component';


@NgModule(
    {
        declarations: [LolComponent,],
        imports: [LolRoutingModule],
        exports: [],
        providers: [],
        bootstrap: []
    })


export class LolModule { }
