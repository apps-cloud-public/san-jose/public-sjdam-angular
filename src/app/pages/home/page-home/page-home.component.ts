import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';


@Component(
{
    selector    : 'app-page-home',
    templateUrl : './page-home.component.html',
    styleUrls   : ['./page-home.component.css'],
    host        : { 'class' : 'ui-section' },
})


export class HomeComponent implements OnInit
{

    constructor(private http: HttpClient) { }

    ngOnInit(): void
    {
        this.http.get('https://jsonplaceholder.typicode.com/comments').subscribe((r) =>
        {
            console.log(r);
        });
    }

}
