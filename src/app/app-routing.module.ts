import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from '@app/pages/index/index.component';


const routes: Routes =
[
    {
        path      : '',
        component :  IndexComponent
    },
    {
        path         : 'home',
        loadChildren : ()=> import('@app/pages/home/home.module')
                           .then(m =>  m.HomeModule),
    },
    {
        path         : 'blog',
        loadChildren : ()=> import('@app/pages/blog/blog.module')
                           .then(m =>  m.BlogModule),
    },
    {
        path         : 'about',
        loadChildren : ()=> import('@app/pages/about/about.module')
                           .then(m =>  m.AboutModule),
    },
    {
        path         : 'lol',
        loadChildren : ()=> import('@app/pages/lol/lol.module')
                           .then(m =>  m.LolModule),
    },
    {
        path       : '**',
        redirectTo : '',
        pathMatch  : 'full',
    },
];

@NgModule(
{
    imports:
    [
        RouterModule.forRoot(routes,
        {
            enableTracing             : false,
            scrollPositionRestoration : 'disabled',
            anchorScrolling           : 'enabled',
            initialNavigation         : 'enabled',
            onSameUrlNavigation       : 'reload'
        })
    ],

    exports : [RouterModule]
})


export class AppRoutingModule { }
