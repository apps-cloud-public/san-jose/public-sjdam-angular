import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoaderComponent } from '@app/components/loader/loader.component';
import { HttpAppInterceptor } from '@app/services/interceptors/http-app.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';


@NgModule(
{
    declarations :
    [
        AppComponent,
        LoaderComponent,
        IndexComponent
    ],

    imports :
    [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule
    ],

    providers :
    [
        {
            provide  : HTTP_INTERCEPTORS,
            useClass : HttpAppInterceptor,
            multi    : true
        }
    ],

    bootstrap : [ AppComponent ]
})


export class AppModule { }
